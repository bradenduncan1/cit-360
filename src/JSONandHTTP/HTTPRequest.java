package JSONandHTTP;

import java.io.*;
import java.net.*;

public class HTTPRequest {


    public static String getHTTPAccess(String string) {

        String content = "";

        try {

            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setRequestMethod("GET");
            http.setRequestProperty("content-type", "application / json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringB = new StringBuilder();

            String line = null;
            while((line = reader.readLine()) != null) {
                stringB.append(line + "\n");
            }
            content = stringB.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static void main(String[] args) {
        System.out.println(HTTPRequest.getHTTPAccess("https://churchofjesuschrist.org"));


    }
}
