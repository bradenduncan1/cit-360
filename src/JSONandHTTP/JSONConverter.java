package JSONandHTTP;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONConverter {

    public static String workoutsToJSON(Workouts workout) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(workout);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }


        return s;
    }

    public static Workouts JSONtoWorkout(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Workouts workout = null;

        try {
            workout = mapper.readValue(s, Workouts.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return workout;
    }

    public static void main(String[] args) {

        Workouts workout = new Workouts();
        workout.setExercise("Bicep curls");
        workout.setSets(4);
        workout.setReps(15);

        String json = JSONConverter.workoutsToJSON(workout);
        System.out.println(json);

        Workouts curls = JSONConverter.JSONtoWorkout(json);
        System.out.println(curls);

    }
}
