package JSONandHTTP;

public class Workouts {

    private String exercise;
    private int sets;
    private int reps;

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public String toString() {
        return "Exercise: " + exercise
                + " Sets: " + sets + " Reps: " + reps;
    }
}
