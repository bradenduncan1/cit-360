package ExceptionsAndValidations;

import java.lang.ArithmeticException;
import java.util.Scanner;

public class DivisionException extends Throwable {

    public static void main(String[] args) throws DivisionException {
    Scanner input = new Scanner(System.in);
    System.out.println("Please enter two integers to divide them");
    int num1 = input.nextInt();
    int num2 = input.nextInt();
    while (num2 == 0) {
        System.out.println("Please enter a number other than 0 to divide by: ");
        num2 = input.nextInt();
        }

    float answer;

        try {
            answer = division(num1, num2);
            System.out.println("Your answer is " + answer);

        } catch(ArithmeticException | DivisionException e) {
            System.out.println("You can not divide by 0. Please enter another integer to divide by: ");
            num2 = input.nextInt();
            answer = division(num1, num2);
            System.out.println("Your answer is " + answer);
        }

    }


    public static float division(int num1, int num2) throws DivisionException {
        float quotient;


        quotient = num1 / num2;

        return quotient;
    }


}
