package JunitTesting;

public class FamilyMember {

    private String name;
    private String relation;

    public FamilyMember() {

    }

    public FamilyMember(String name, String relation) {
        this.name = name;
        this.relation = relation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String toString() {
        return "Name: " + name + " , Relation; " + relation;
    }
}
