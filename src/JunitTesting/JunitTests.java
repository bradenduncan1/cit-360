package JunitTesting;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class JunitTests {



    private static FamilyMember fam1 = new FamilyMember("Camryn", "Wife");

    private static FamilyMember fam2 = new FamilyMember("Paxton", "Son");

    @Test
    public void assertion1() {
        assertNotNull(fam1);
        assertNotNull(fam2);
    }


    @Test
    public void assertion2() {
        assertEquals(fam1.getName(), "Camryn");
        assertEquals(fam2.getName(), "Paxton");
    }

    @Test
    public void assertion3() {
        assertNotEquals(fam1.getRelation(), "Husband");
        assertNotEquals(fam2.getRelation(), "Daughter");
    }

    public static void main(String[] args) {

        System.out.println("This is my family.");

        FamilyMember me = new FamilyMember("Braden" , "myself");
        try {
            assertNotSame(me, fam1);
            assertNotSame(me, fam2);
            assertNotSame(fam1, fam2);

        } catch(Exception e) {
            System.err.println(e.toString());
        }

        System.out.println("My family goes: " + me.toString() + ", " + fam1.toString() + ", " + fam2.toString());
    }


}

