package Hibernate;

import javax.persistence.*;


@Entity
@Table(name = "family")

public class Family {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfamily")
    private int id;

    @Column(name = "relation")
    private String relation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String toString() {
        return Integer.toString(id) + " " + relation;
    }



}
