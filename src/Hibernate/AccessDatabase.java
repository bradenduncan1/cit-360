package Hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class AccessDatabase {

    SessionFactory factory = null;
    Session session = null;

    private static AccessDatabase singleInstance = null;

    private AccessDatabase() {
        factory = HibernateSetup.getSessionFactory();
    }

    public static AccessDatabase getInstance() {
        if (singleInstance == null) {
            singleInstance = new AccessDatabase();
        }
        return singleInstance;
    }

    public Family getFamilyMember(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Family where id = " + Integer.toString(id);
            Family member = (Family)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return member;

        } catch (HibernateException e) {
            System.out.println(e.toString());
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Family addFamilyMember() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.createQuery("from Hibernate.Family");
            Family mem = new Family();
            mem.setId(4);
            mem.setRelation("puppy");

            session.save(mem);
            session.getTransaction().commit();
            return mem;
        } catch (HibernateException e) {
            System.out.println(e.toString());
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public List<Family> getFamily() {

        try {
        session = factory.openSession();
        session.getTransaction().begin();
        String sql = "from Hibernate.Family";
        List<Family> fam = (List<Family>)session.createQuery(sql).getResultList();
        session.getTransaction().commit();
        return fam;

    } catch (HibernateException e) {
        System.out.println(e.toString());
        session.getTransaction().rollback();
        return null;
    } finally {
        session.close();
    }

    }
}
