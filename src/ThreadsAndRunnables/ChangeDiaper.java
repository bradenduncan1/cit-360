package ThreadsAndRunnables;

import java.util.Random;

public class ChangeDiaper implements Runnable {

    private String parent;
    private int num;
    private String task;


    public ChangeDiaper(String parent, int num, String task) {
        this.parent = parent;
        this.num = num;
        this.task = task;
    }

    Random random = new Random();
    int rand = random.nextInt(50);


    public void run() {
        System.out.println("\nExecuting whose turn it will be to take care of the baby: \n");
        for (int turn = 1; turn < rand; turn++) {
            if(num % turn == 0) {
                System.out.println("It is " + parent + "'s turn to " + task + "\n");
            try {
                Thread.sleep(50);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
            }
        }
        System.out.println("\n" + parent + " is done taking their turn.\n");
    }
}
