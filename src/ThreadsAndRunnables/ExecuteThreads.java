package ThreadsAndRunnables;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThreads {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        ChangeDiaper cd1 = new ChangeDiaper("Dad", 42, "change baby's diaper");
        ChangeDiaper cd2 = new ChangeDiaper("Mom", 27, "change baby's diaper");

        myService.execute(cd1);
        myService.execute(cd2);

    }
}
