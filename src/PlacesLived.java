import java.util.Comparator;

class PlacesLived {

    protected String city;
    protected String state;

    public PlacesLived(String city, String state) {
        this.city = city;
        this.state = state;
    }

    public String toString() {
        return city + ", " + state;
    }
}

class SortedStates implements Comparator<PlacesLived> {

    @Override public int compare(PlacesLived a, PlacesLived b) {
        return a.state.compareTo(b.state);
    }



}
