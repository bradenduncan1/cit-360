import java.util.*;

public class Collections {
    /* Class to show examples of different collections
     I will use a list, queue, set, and tree to demonstrate an understanding of collections
     */


    public static void main(String[] args) {

        System.out.println("--- Array List ---");
        int counter = 0;
        System.out.println("Please enter 5 modes of transportation");
        Scanner input = new Scanner(System.in);
        List list = new ArrayList();
            for (int i = 0; i < 5; i++) {
                String mode = input.nextLine();
                if(mode.isEmpty()) {
                    System.out.println("Please enter a mode of transportation");
                    i--;
                }
                else {
                    list.add(mode);
                }
        }

        for (Object str : list)
        {
            System.out.println((String) str);
        }

        System.out.println("\n--- List using Generics ---");

        ArrayList homes = new ArrayList <PlacesLived>();
        homes.add(new PlacesLived("Tampa", "Florida"));
        homes.add(new PlacesLived("West Valley", "Utah"));
        homes.add(new PlacesLived("Tulsa", "Oklahoma"));
        homes.add(new PlacesLived("Newnan", "Georgia"));
        homes.add(new PlacesLived("Albuquerque", "New Mexico"));
        homes.add(new PlacesLived("Rexburg", "Idaho"));

        for (Object home: homes) {
            System.out.println(home);
        }

        System.out.println("\n--- sorted list using Comparator interface ---");

        java.util.Collections.sort(homes, new SortedStates());
//SortedStates() gives us a compareTo method that will allow us to compare the first letters of each State
        for (Object home: homes) {
            System.out.println(home);
        }


        System.out.println("\n--- Set ---");
//TreeSets will organize our set by alphabetical and capitalization

        Set set = new TreeSet();
        set.add("Pizza");
        set.add("Is");
        set.add("good");
        set.add("for");
        set.add("any");
        set.add("meal");
        set.add("Pizza");

        for (Object str: set) {
            System.out.println(str);
        }



        System.out.println("\n--- Queue ---");
//This will display the queue in alphabetical and caps order
// so the output will not make much sense even though it is typed in order

        Queue queue = new PriorityQueue();
        queue.add("Pizza");
        queue.add("is");
        queue.add("good");
        queue.add("For");
        queue.add("any");
        queue.add("Meal");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }


        System.out.println("\n--- Map ---");

        Map map = new HashMap();
        map.put(1, "Pizza");
        map.put(5, "is");
        map.put(6, "good");
        map.put(2, "For");
        map.put(3, "any");
        map.put(4, "Meal");

        for(int i = 1; i < 7; i++) {
            String print = (String)map.get(i);
            System.out.println(print);
        }


    }

}
